local.dependencies:
	docker compose up database -d --wait

local.test: local.dependencies
	./gradlew check jibDockerBuild --stacktrace
	make local.clean

local.cluster:
	./gradlew jibDockerBuild
	docker compose up

local.clean:
	docker compose down

kind.create:
	# version is fixed because of https://github.com/zalando/postgres-operator/issues/2098
	kind create cluster --name spring-boot-template --image=kindest/node:v1.23.13
	kubectl cluster-info --context kind-spring-boot-template

kind.dependencies:
	helm repo add postgres-operator-charts https://opensource.zalando.com/postgres-operator/charts/postgres-operator
	helm install postgres-operator postgres-operator-charts/postgres-operator
	kubectl apply -f kubernetes/postgres.yaml

kind.application:
	./gradlew jibDockerBuild
	kind load docker-image spring-boot-template --name spring-boot-template
	kubectl apply -f kubernetes/deployment.yaml
	kubectl apply -f kubernetes/service.yaml

kind.port-forward:
	kubectl port-forward svc/challenge-application 8080:80

kind.clean:
	kind delete cluster --name spring-boot-template
