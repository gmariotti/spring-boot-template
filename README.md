# Spring Boot Template

## Implementation details - TODO

Details about [CHALLENGE.md](CHALLENGE.md) and how it was solved

## Run locally

### IntelliJ

With IJ Ultimate, create a `Spring Boot` configuration and set profile to `local`.

With IJ CE, create a `Kotlin` configuration with the following values

    Main class:             challenge.ApplicationKt
    Environment variables:  SPRING_PROFILES_ACTIVE=local

### Testing

Testing can be run with

    $ ./gradlew test

### Docker

The image can be built locally with `

    $ ./gradlew jibDockerBuild

The entire cluster can be started with

    $ make local.cluster

### Kubernetes

[Kind](https://kind.sigs.k8s.io/) can be used to create a cluster with all the necessary operators
```
$ make kind.create

# give time to the containers to be properly created and running
$ make kind.dependencies

# create application resources
$ make kind.application

# remove the cluster
$ make kind.clean
```

It's possible to connect to the application deployed in the cluster using `port-forward`.
```
$ make kind.port-forward

# in a separate shell
$ http localhost:8080/health
$ http localhost:8080/example
```