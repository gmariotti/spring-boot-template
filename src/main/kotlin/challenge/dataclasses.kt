@file:Suppress("ktlint:filename")

package challenge

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonCreator.Mode.DELEGATING
import com.fasterxml.jackson.annotation.JsonValue

data class SinglePropertyClass
@JsonCreator(mode = DELEGATING)
constructor(@JsonValue val value: String) {
    override fun toString(): String = value
}

data class AnotherPropertyClass
@JsonCreator(mode = DELEGATING)
constructor(@JsonValue val value: String) {
    override fun toString(): String = value
}
