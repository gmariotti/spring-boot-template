package challenge

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class Controller() {

    @GetMapping("/example")
    suspend fun get(@RequestParam("is_failure") isFailure: Boolean = false): Success {
        return if (isFailure) throw ExampleProblemException() else Success(value = "ok")
    }
}

data class Success(val value: String)
