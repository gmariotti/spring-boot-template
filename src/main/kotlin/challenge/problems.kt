@file:Suppress("ktlint:filename", "FunctionName")

package challenge

import org.springframework.http.HttpStatus
import org.springframework.http.ProblemDetail
import org.springframework.web.ErrorResponseException
import java.net.URI

class ExampleProblemException : ErrorResponseException(
    HttpStatus.NOT_FOUND,
    ProblemDetail
        .forStatusAndDetail(HttpStatus.NOT_FOUND, "With a certain description")
        .apply {
            type = URI.create("/problems/example-problem")
            title = "There's a problem"
        },
    null,
)

fun AnotherProblemException(): ProblemDetail = ProblemDetail
    .forStatusAndDetail(HttpStatus.INTERNAL_SERVER_ERROR, "With a certain description")
    .apply {
        type = URI.create("/problems/another-problem")
        title = "There's a problem"
    }
