package challenge

import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.actuate.observability.AutoConfigureObservability
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@AutoConfigureObservability
class ApplicationTest {
    @Autowired
    lateinit var client: WebTestClient

    @ParameterizedTest
    @ValueSource(
        strings = ["/health/liveness", "/health/readiness", "/prometheus"],
    )
    fun `verify expected endpoints are exposed`(endpoint: String) {
        client
            .get()
            .uri(endpoint)
            .exchange()
            .expectStatus().is2xxSuccessful
    }

    @Test
    fun `verify 2xx on example`() {
        client
            .get()
            .uri("/example")
            .exchange()
            .expectStatus().is2xxSuccessful
    }

    @Test
    fun `verify returns 2xx`() {
        client
            .get()
            .uri("/example")
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody().json("""{"value": "ok"}""")
    }

    @Test
    fun `verify problem on failure example`() {
        client
            .get()
            .uri("/example?is_failure=true")
            .exchange()
            .expectStatus().is4xxClientError
            .expectHeader().contentType(MediaType.APPLICATION_PROBLEM_JSON)
            .expectBody()
            .json(
                """{
                "type": "/problems/example-problem",
                "title": "There's a problem",
                "status": 404,
                "detail": "With a certain description"
            }""",
            )
    }
}
