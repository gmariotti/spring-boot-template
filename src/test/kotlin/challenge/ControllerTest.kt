package challenge

import challenge.configurations.ProblemConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.annotation.Import
import org.springframework.test.web.reactive.server.WebTestClient

@WebFluxTest(controllers = [Controller::class])
@Import(ProblemConfiguration::class)
class ControllerTest {
    @Autowired
    lateinit var client: WebTestClient
}
